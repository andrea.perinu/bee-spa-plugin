import React, { FC } from 'react';

import { ILayoutProps } from '../../types';
import classes from './Layout.module.css';

const Layout: FC<ILayoutProps> = (props: ILayoutProps) => {
	const { header, footer, children } = props;

	return (
		<div className={classes.Layout}>

			<header className={classes.Header}>
				<h1 className={classes.HeaderText}>{header}</h1>
			</header>

			<main className={classes.Content}>{children}</main>

			<footer className={classes.Footer}>{footer}</footer>

		</div>
	);
};

export default Layout;