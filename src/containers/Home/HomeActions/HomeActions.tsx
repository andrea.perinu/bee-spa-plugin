import React, { FC, useRef } from 'react';

import template from '../../../base-m-bee.json';
import { IHomeActionsProps } from '../../../types';
import classes from './HomeActions.module.css';

const HomeActions: FC<IHomeActionsProps> = (props: IHomeActionsProps) => {
	const { beeWidget, loading } = props;

	const inputEl = useRef(null);

	const onPreview = () => beeWidget.preview();
	const onSaveHtml = () => beeWidget.send();
	const onSaveJson = () => beeWidget.saveAsTemplate();

	const onLoadTemplate = (e: any) => {
		var reader = new FileReader();
		reader.onload = onReaderLoad;
		reader.readAsText(e.target.files[0]);
	};

	const onReaderLoad = (e: any) => {
		const templateToLoad = e.target.result;
		templateToLoad && beeWidget.load(JSON.parse(templateToLoad));
	};

	return (
		<div>
			<h4><strong>Custom actions:</strong></h4>

			<div className={classes.Actions}>
				<button className={`btn btn-primary ${classes.Action}`} onClick={onPreview} disabled={loading}>
					Preview
				</button>

				<button className={`btn btn-primary ${classes.Action}`} onClick={onSaveJson} disabled={loading}>
					Save JSON
				</button>

				<button className={`btn btn-primary ${classes.Action}`} onClick={onSaveHtml} disabled={loading}>
					Save HTML
				</button>

				<input
					hidden type="file" id="import-profile" accept="application/json"
					ref={inputEl} onChange={onLoadTemplate}
				/>
				<button
					className={`btn btn-primary ${classes.Action}`} disabled={loading}
					onClick={() => (inputEl.current as any).click()}
				>
					Load template
				</button>

				<button
					className={`btn btn-primary ${classes.Action}`}
					onClick={(_: any) => beeWidget.load(template)} disabled={loading}
				>
					Load default template
				</button>

				<button
					className={`btn btn-primary ${classes.Action}`}
					onClick={(_: any) => beeWidget.toggleStructure()} disabled={loading}
				>
					Toggle structure
				</button>
			</div>

		</div>
	);
}

export default HomeActions;