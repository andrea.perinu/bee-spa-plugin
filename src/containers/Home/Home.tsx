import React, { FC, useEffect, useState } from 'react';

import template from '../../base-m-bee.json';
import Bee from '../../Bee';
import { IBeeConfig, IBeePlugin } from '../../types';
import { downloadHtmlFile, downloadJsonFile } from '../../utility';
import classes from './Home.module.css';
import HomeActions from './HomeActions/HomeActions';

const clientId = '829e198c-bc00-47a7-a439-2ca7e5519484';
const clientSecret = 'nmuOBtOJOWtfAWc0F6qad587tZasO5TvM5WzvskvBUH7ouNXP8oW';

const beeWidget = new Bee() as IBeePlugin;

let canDownloadAssets = true;

const Home: FC = () => {
	const [loading, setLoading] = useState(true);

	const onSend = (htmlFile: string): void => {
		canDownloadAssets && downloadHtmlFile(htmlFile);
	}

	const onSaveAsTemplate = (jsonFile: string): void => {
		canDownloadAssets && downloadJsonFile(jsonFile);
		localStorage.setItem('configJson', jsonFile);
	};
	const onLoad = (_: any): void => setLoading(false);

	const onSave = (jsonFile: string, htmlFile: string): void => {
		if (canDownloadAssets) {
			downloadJsonFile(jsonFile);
			downloadHtmlFile(htmlFile);
		}
		localStorage.setItem('configJson', jsonFile);
	};

	const onAutoSave = (jsonFile: string) => localStorage.setItem('configJson', jsonFile);

	useEffect(() => {
		const beeConfig = {
			uid: 'CmsUserName', container: 'bee-plugin-container', autosave: 30,
			language: 'en-US', onSave, onSaveAsTemplate, onSend, onLoad, onAutoSave
		} as IBeeConfig;

		beeWidget.getToken(clientId, clientSecret).then(() => {
			const savedTemplate = localStorage.getItem('configJson');
			const templateToLoad = savedTemplate ? JSON.parse(savedTemplate) : template;
			beeWidget.start(beeConfig, templateToLoad);
		});
	}, []);

	return (
		<>
			<div className={`form-check form-check-inline ${classes.AssetsWrapper}`}>
				<input
					type="checkbox" className="form-check-input" id="assets-checkbox"
					onChange={(_: any) => { canDownloadAssets = !canDownloadAssets }}
					defaultChecked={canDownloadAssets}
				/>
				<label className="form-check-label" htmlFor="assets-checkbox">Download assets on save</label>
			</div>

			<HomeActions beeWidget={beeWidget} loading={loading}></HomeActions>

			<div className={classes.PluginWrapper}>
				<article className={classes.PluginContainer} id="bee-plugin-container"></article>
			</div>
		</>
	);
}

export default Home;
