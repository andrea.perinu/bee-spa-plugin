const downloadFile = (fileData: string, ext: string) => {
	var link = document.createElement('a');

	if (link.download !== undefined) {
		const fileDataFormatted = `data:text/${ext};charset=utf-8,${encodeURIComponent(fileData)}`;
		link.setAttribute('href', fileDataFormatted);
		link.setAttribute('download', `data.${ext}`);
		link.style.visibility = 'hidden';
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
};

export const downloadJsonFile = (jsonFileData: string) => downloadFile(jsonFileData, 'json');
export const downloadHtmlFile = (htmlFileData: string) => downloadFile(htmlFileData, 'html');