export interface IBeePlugin {
	getToken: (clientID: string, clientSecret: string) => any;
	start: (beeConfig: IBeeConfig, templateToLoad: any) => any;
	load: (template: object) => any;
	save: () => { jsonFile: string, htmlFile: string };
	saveAsTemplate: () => { jsonFile: string };
	send: () => { htmlFile: string; }
	preview: () => void;
	toggleStructure: () => void;
	onLoad: () => any;
}

export interface IBeeConfig {
	uid: string;
	container: string;
	autosave: number;
	language: string;
	onSave: (jsonFile: string, htmlFile: string) => void;
	onSaveAsTemplate: (jsonFile: string) => void;
	onAutoSave: () => void;
	onSend: (htmlFile: string) => void;
	onLoad: () => any;
}

export interface IHomeActionsProps {
	loading: boolean;
	beeWidget: IBeePlugin;
}

export interface ILayoutProps {
	header: string;
	footer: string;
	children: React.ReactNode;
}