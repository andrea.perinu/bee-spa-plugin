import React, { FC } from 'react';

import Home from './containers/Home/Home';
import Layout from './components/Layout/Layout';

const App: FC = () => {
	return (
		<Layout
			header="Fancy BEE SPA"
			footer="Fancy BEE SPA built with ♥ and powered by React, Typescript, Bootstrap and Webpack."
		>
			<Home />
		</Layout>
	);
}

export default App;
