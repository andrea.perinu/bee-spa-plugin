# bee-spa-plugin

## Contents
- [Introduction](#introduction)
- [Directory layout](#directory-layout)
	* [The build directory](#the-build-directory)
	* [The public directory](#the-public-directory)
	* [The src directory](#the-src-directory)
	* [The components directory](#the-components-directory)
	* [The containers directory](#the-containers-directory)
- [Scripts](#scripts)
	* [yarn start](#yarn-start)
	* [yarn build](#yarn-build)

## Introduction
I developed the SPA following the Data Challenge guidelines. Plus I added extra BEE Plugin APIs like: loadTemplate and toggleStructure. I also added a button to load the base (with correct image paths) template.
The template is stored in the local storage, this way it is saved on app reaload. I also connected the autoSave to localStorage, in order to save also over time.
Plus I added an interface flag that let you download the JSON and HTML documents.
I used authentication client side, that I don't like, a better idea should be create a simple BE server that expose the token to the client, without having the clientId and clientSecret stubbed on FE code.


## Directory layout
The project has the following structure:
```
.
├── build
│   └── static
│       ├── css
│       └── js
├── public
└── src
    ├── components
    │   └── Layout
    └── containers
        └── Home
            └── HomeActions
```

### Top level directory
Top level directory contains:

* **_App.tsx_**					main application react node
* **_base-m-bee.json_**			base BEE Plugin template
* **_index.css_**				base application styles
* **_index.tsx_**				React index file
* **_types.ts_**				interfaces file
* **_utility.ts_**				application utility tools
* **_README.md_**				this file

### The **_build_** directory
The _build_ directory contains the application distribution files.

### The **_public_** directory
The _public_ directory contains the application main .html file and the robot.

### The **_src_** directory
The _src_ directory contains the application source files.

### The **_components_** directory
The _components_ directory contains reusable, agnostic components.

### The **_containers_** directory
The _containers_ directory contains the application ad hoc custom components.


## Scripts
In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
